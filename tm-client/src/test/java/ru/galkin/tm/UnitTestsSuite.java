package ru.galkin.tm;

import org.junit.experimental.categories.Categories;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import ru.galkin.tm.marker.IUnitTests;
import ru.galkin.tm.service.ProjectServiceTest;

@RunWith(Categories.class)
@Categories.IncludeCategory(IUnitTests.class)
@Suite.SuiteClasses({ProjectServiceTest.class})
public class UnitTestsSuite {
}
