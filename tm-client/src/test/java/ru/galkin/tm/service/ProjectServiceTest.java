package ru.galkin.tm.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.galkin.tm.endpoint.Project;
import ru.galkin.tm.endpoint.ProjectEndpoint;
import ru.galkin.tm.endpoint.ProjectEndpointService;
import ru.galkin.tm.marker.ISoapTests;

public class ProjectServiceTest {

    private final ProjectEndpointService projectEndpointService = new ProjectEndpointService();
    private final ProjectEndpoint projectEndpoint = projectEndpointService.getProjectEndpointPort();

    @Before
    @Category(ISoapTests.class)
    public void onStart()
    {
        projectEndpoint.clearProject();
    }

    @Test
    @Category(ISoapTests.class)
    public void CreateTest()
    {
        Assert.assertTrue(projectEndpoint.findAllProject().isEmpty());
        projectEndpoint.createProject("ProjecTest1");
        Assert.assertFalse(projectEndpoint.findAllProject().isEmpty());
        Assert.assertNull(projectEndpoint.findByNameProject("ProjecTest2"));
    }
    @Test
    @Category(ISoapTests.class)
    public void UpdateTest()
    {
        Assert.assertTrue(projectEndpoint.findAllProject().isEmpty());
        projectEndpoint.createProject("ProjecTest1");
        Assert.assertNotNull(projectEndpoint.findByNameProject("ProjecTest1"));
        Project project1 = projectEndpoint.findByNameProject("ProjecTest1");
        projectEndpoint.updateProject(project1.getId(), "NewProjectTest1Name", "NewProjectTest1Description");
        Assert.assertNotNull(projectEndpoint.findByNameProject("NewProjectTest1Name"));
        Project project2 = projectEndpoint.findByNameProject("NewProjectTest1Name");
        Assert.assertEquals(project1.getId(), project2.getId());
        Assert.assertEquals("NewProjectTest1Description", project2.getDescription());
    }
}
