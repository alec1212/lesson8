package ru.galkin.tm.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.galkin.tm.endpoint.*;
import ru.galkin.tm.marker.ISoapTests;

public class TaskServiceTest {

    private final TaskEndpointService projectEndpointService = new TaskEndpointService();
    private final TaskEndpoint projectEndpoint = projectEndpointService.getTaskEndpointPort();

    @Before
    @Category(ISoapTests.class)
    public void onStart()
    {
        projectEndpoint.clearTask();
    }

    @Test
    @Category(ISoapTests.class)
    public void CreateTest()
    {
        Assert.assertTrue(projectEndpoint.findAllTask().isEmpty());
        projectEndpoint.createTask("TaskTest1");
        Assert.assertFalse(projectEndpoint.findAllTask().isEmpty());
        Assert.assertNull(projectEndpoint.findByNameTask("TaskTest2"));
    }
    @Test
    @Category(ISoapTests.class)
    public void UpdateTest()
    {
        Assert.assertTrue(projectEndpoint.findAllTask().isEmpty());
        projectEndpoint.createTask("TaskTest1");
        Assert.assertNotNull(projectEndpoint.findByNameTask("TaskTest1"));
        Task project1 = projectEndpoint.findByNameTask("TaskTest1");
        projectEndpoint.updateTask(project1.getId(), "NewTaskTest1Name", "NewTaskTest1Description");
        Assert.assertNotNull(projectEndpoint.findByNameTask("NewTaskTest1Name"));
        Task project2 = projectEndpoint.findByNameTask("NewTaskTest1Name");
        Assert.assertEquals(project1.getId(), project2.getId());
        Assert.assertEquals("NewTaskTest1Description", project2.getDescription());
    }
}
