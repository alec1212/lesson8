package ru.galkin.tm;

import ru.galkin.tm.endpoint.Project;
import ru.galkin.tm.endpoint.ProjectEndpoint;
import ru.galkin.tm.endpoint.ProjectEndpointService;

public class client {

    public static void main(String [] args)
    {
        ProjectEndpointService projectEndpointService = new ProjectEndpointService();
        ProjectEndpoint projectEndpoint = projectEndpointService.getProjectEndpointPort();
        System.out.println(projectEndpoint.findAllProject() );

        for (Project p: projectEndpoint.findAllProject()){

            System.out.println(p.getName());
        }
    }

}
