package ru.galkin.tm.dao;

import ru.galkin.tm.entity.Task;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

@Stateless
public class TaskDao {

    @PersistenceContext
    private EntityManager em;

    public Task persist(Task task) {
        em.persist(task);
        return task;
    }

    public Task merge(Task task) {
        em.merge(task);
        return task;
    }

    public Task remove(Task task) {
        em.remove(task);
        return task;
    }

    public Task removeById(String id) {
        final Task task = findById(id);
        if (task == null) return null;
        return remove(task);
    }

    public Task findById(String id) {
        return em.find(Task.class, id);
    }

    public Task findByName(String name) {
        final TypedQuery<Task> query = em.createQuery("FROM Task WHERE name = :name ", Task.class)
                .setParameter("name", name);
        Task task = query.getSingleResult();
        return task;
    }

    public List<Task> findAll() {

        return em.createQuery("from Task", Task.class).getResultList();
    }

    public void clear() {
        em.createQuery("delete from Task").executeUpdate();
    }


}
