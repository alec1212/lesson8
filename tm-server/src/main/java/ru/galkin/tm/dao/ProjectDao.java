package ru.galkin.tm.dao;

import ru.galkin.tm.entity.Project;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

@Stateless
public class ProjectDao {

    @PersistenceContext
    private EntityManager em;

    public Project persist(Project project) {
        em.persist(project);
        return project;
    }

    public Project merge(Project project) {
        em.merge(project);
        return project;
    }

    public Project remove(Project project) {
        em.remove(project);
        return project;
    }

    public Project removeById(String id) {
        final Project project = findById(id);
        if (project == null) return null;
        return remove(project);
    }

    public Project findById(String id) {
        return em.find(Project.class, id);
    }

    public Project findByName(String name) {
        final TypedQuery<Project> query = em.createQuery("FROM Project WHERE name = :name ", Project.class)
                .setParameter("name", name);
        Project project = query.getSingleResult();
        return project;
    }

    public List<Project> findAll() {

        return em.createQuery("from Project", Project.class).getResultList();
    }

    public void clear() {
        em.createQuery("delete from Project").executeUpdate();
    }


}
