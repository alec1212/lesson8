package ru.galkin.tm.endpoint;

import ru.galkin.tm.entity.Task;
import ru.galkin.tm.service.TaskService;

import javax.inject.Inject;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public  class TaskEndpoint {

    @Inject
    private   TaskService taskService;

    @WebMethod
    public Task createTask(@WebParam(name = "name")String name) {
        return taskService.create(name);
    }

    @WebMethod
    public Task findByNameTask(@WebParam(name = "name")String name) {
        return taskService.findByName(name);
    }
    @WebMethod
    public Task removeByIdTask(@WebParam(name = "id")String id) {
        return taskService.removeById(id);
    }
    @WebMethod
    public Task removeByNameTask(@WebParam(name = "name")String name) {
        return taskService.removeByName(name);
    }
    @WebMethod
    public Task findByIdTask(@WebParam(name = "id")String id) {
        return taskService.findById(id);
    }
    @WebMethod
    public void clearTask() {
        taskService.clear();
    }
    @WebMethod
    public List<Task> findAllTask() {
        return taskService.findAll();
    }

    @WebMethod
    public Task updateTask(@WebParam(name = "id")String id,
                                 @WebParam(name = "name")String name,
                                 @WebParam(name = "description")String description)  {
        return taskService.update(id, name, description);
    }
}
