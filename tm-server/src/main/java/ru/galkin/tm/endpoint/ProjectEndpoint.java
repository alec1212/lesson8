package ru.galkin.tm.endpoint;

import ru.galkin.tm.entity.Project;
import ru.galkin.tm.service.ProjectService;

import javax.inject.Inject;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public class ProjectEndpoint {

    @Inject
    public ProjectService projectService;

    @WebMethod
    public Project createProject(@WebParam(name = "name")String name) {
        return projectService.create(name);
    }



    @WebMethod
    public Project create(String name, String description) {
        return projectService.create(name, description);
    }

    @WebMethod
    public Project updateProject(@WebParam(name = "id")String id,
                                 @WebParam(name = "name")String name,
                                 @WebParam(name = "description")String description)  {
        return projectService.update(id, name, description);
    }

    @WebMethod
    public void clearProject() {
        projectService.clear();
    }


    @WebMethod
    public Project findByNameProject(@WebParam(name = "name")String name)  {
        return projectService.findByName(name);
    }
    @WebMethod
    public Project findByIdProject(@WebParam(name = "id") String id)  {
        return projectService.findById(id);
    }

    @WebMethod
    public Project removeByIdProject(@WebParam(name = "id") String id)  {
        return projectService.removeById(id);
    }
    @WebMethod
    public Project removeByNameProject(@WebParam(name = "name")String name)  {
        return projectService.removeByName(name);
    }
    @WebMethod
    public List<Project> findAllProject()  {
        return projectService.findAll();
    }

}
