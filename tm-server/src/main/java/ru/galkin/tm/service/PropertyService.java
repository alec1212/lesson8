package ru.galkin.tm.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertyService {

    private static final String FILE = "/application.properties";


    private static final InputStream INPUT_STREAM = PropertyService.class.getResourceAsStream(FILE);

    private static final Properties PROPERTIES = new Properties();

    static {

        try {
            PROPERTIES.load(INPUT_STREAM);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public String getHost() {
        return PROPERTIES.getProperty("db.host");
    }

    public String getPort() {
        return PROPERTIES.getProperty("db.port");
    }

    public String getDatabase() {
        return PROPERTIES.getProperty("db.database");
    }

    public String getUsername() {
        return PROPERTIES.getProperty("db.username");
    }

    public String getPassword() {
        return PROPERTIES.getProperty("db.password");
    }



}
