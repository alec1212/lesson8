package ru.galkin.tm.service;

import ru.galkin.tm.dao.ProjectDao;
import ru.galkin.tm.entity.Project;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.List;

@Stateless
public class ProjectService {


    @Inject
    private ProjectDao projectDao;


    public ProjectService() {
    }

    public Project create(String name) {
        if (name == null || name.isEmpty()) return null;
        Project newProject = new Project();
        newProject.setName(name);
        return projectDao.persist(newProject);
    }


    public Project create(String name, String description) {
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        Project newProject = new Project();
        newProject.setName(name);
        newProject.setDescription(description);
        return projectDao.persist(newProject);
    }

    public Project update(String id, String name, String description) {
        if (id == null) return null;
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;

        Project project = new Project();
        project.setId(id);
        project.setName(name);
        project.setDescription(description);
        return projectDao.merge(project);
    }

    public void clear() {
        projectDao.clear();
    }


    public Project findByName(String name) {
        if (name == null || name.isEmpty()) return null;
        return projectDao.findByName(name);
    }


    public Project findById(String id) {
        return projectDao.findById(id);
    }

    public Project removeById(String id) {
        return projectDao.removeById(id);
    }

    public Project removeByName(String name) {
        if (name == null || name.isEmpty()) return null;
        Project project = findByName(name);
        return projectDao.remove(project);

    }


    public List<Project> findAll() {
        return projectDao.findAll();
    }

}
