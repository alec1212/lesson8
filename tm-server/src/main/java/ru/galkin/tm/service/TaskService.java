package ru.galkin.tm.service;

import ru.galkin.tm.dao.TaskDao;
import ru.galkin.tm.entity.Task;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.List;

@Stateless
public class TaskService {

    @Inject
    private TaskDao taskDao;


    public Task create(String name) {
        if (name == null || name.isEmpty()) return null;
        Task newTask = new Task();
        newTask.setName(name);
        return taskDao.persist(newTask);
    }


    public Task create(String name, String description) {
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        Task newTask = new Task();
        newTask.setName(name);
        newTask.setDescription(description);
        return taskDao.persist(newTask);
    }

    public Task update(String id, String name, String description) {
        if (id == null) return null;
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;

        Task task = new Task();
        task.setId(id);
        task.setName(name);
        task.setDescription(description);
        return taskDao.merge(task);
    }

    public void clear() {
        taskDao.clear();
    }


    public Task findByName(String name) {
        if (name == null || name.isEmpty()) return null;
        return taskDao.findByName(name);
    }


    public Task findById(String id) {
        return taskDao.findById(id);
    }

    public Task removeById(String id) {
        return taskDao.removeById(id);
    }

    public Task removeByName(String name) {
        if (name == null || name.isEmpty()) return null;
        Task task = findByName(name);
        return taskDao.remove(task);

    }


    public List<Task> findAll() {
        return taskDao.findAll();
    }
}
